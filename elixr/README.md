# Elixir Simple BuzzFizz using a GenServer

# Overview
This is a simple application that runs through 100 numbers sending messages to a GenServer that stores the state of the current number according to the standard BuzzFizz rules. It then prints this out after each number is sent. 

## Next to learn
To take this further I'd need to explore the language more, I'd do this specifically by: 
- Rewriting this using a pure GenStage implementation, 
- Going through the syntax structures of Elixir - I.e. writing case, if, pattern matching statements. 
- I'd then write this using the extractor library that provides ExActor.
- Re write the entire thing in Erlang to see how it works under the hood and gain a better understanding
- Play with more advanced features such as macros and state recovery mechanisms in Elixir. 

# Running the program
Execute command 'mix run' (after dependencies have been built [mix.deps get,compile])

# Example output
Starting...
Started.
Sending 100 numbers to the fizzbuzzer...
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
17
Fizz
19
Buzz
Fizz
22
23
Fizz
Buzz
26
Fizz
28
29
FizzBuzz
31
32
Fizz
34
Buzz
Fizz
37
38
Fizz
Buzz
41
Fizz
43
44
FizzBuzz
46
47
Fizz
49
Buzz
Fizz
52
53
Fizz
Buzz
56
Fizz
58
59
FizzBuzz
61
62
Fizz
64
Buzz
Fizz
67
68
Fizz
Buzz
71
Fizz
73
74
FizzBuzz
76
77
Fizz
79
Buzz
Fizz
82
83
Fizz
Buzz
86
Fizz
88
89
FizzBuzz
91
92
Fizz
94
Buzz
Fizz
97
98
Fizz
Buzz
Sent 100 numbers to fizzbuzzer.
Stopping...
Stopped.