defmodule FizzBuzzServer do
  use ExActor.GenServer

  defstart start_link() do
    initial_state(0)
  end

  defcast fizzbuzz(x), state: state, do: new_state(fizzbuzzer(state, x))

  defcall get, state: state, do: reply(state)

  defcast stop, do: stop_server(:normal)

  def fizzbuzzer(state, x) do
    argument_patterns = fn
      (0, 0, _) -> "FizzBuzz"
      (0, _, _) -> "Fizz"
      (_, 0, _) -> "Buzz"
      (_, _, x) -> x
    end

    fizzbuzz = fn (n) -> 
      argument_patterns.(rem(n, 3), rem(n, 5), n)
    end

    state = fizzbuzz.(x)
  end

end

defmodule App do
  use Application
  
  def start(_type, _args) do
    IO.puts "Starting..."
    {:ok, fizzbuzz} = FizzBuzzServer.start_link
    IO.puts "Started."

    IO.puts "Sending 100 numbers to the fizzbuzzer..."
    Enum.each(1..100, fn(i) -> 
      FizzBuzzServer.fizzbuzz(fizzbuzz, i); 
      IO.puts FizzBuzzServer.get(fizzbuzz) 
    end)
    IO.puts "Sent 100 numbers to fizzbuzzer."
    
    IO.puts "Stopping..."
    FizzBuzzServer.stop(fizzbuzz)
    IO.puts "Stopped."
  end
end 

