# coding: utf-8
import mistune
from textworld import TextWorld


def parseFileToList(file):
    fileToParse = open(file, 'r')
    fileString = fileToParse.read()
    lexer = mistune.Markdown()
    return lexer.block(fileString)


def parseListToBlocks(list):
    allBlocks = []
    currentBlock = []
    firstHeadingHit = False
    for x in list:
        if x['type'] == 'heading':
            x['title'] = x['text']
            if not firstHeadingHit:
                firstHeadingHit = True
            else:
                allBlocks.append(currentBlock)
                currentBlock = []
        currentBlock.append(x)
    allBlocks.append(currentBlock)
    return allBlocks


def parse_and_generate(filename):
    tokenList = parseFileToList(filename)
    blocks = parseListToBlocks(tokenList)
    world = TextWorld(blocks)
    return world.generate()


if __name__ == '__main__':
    # Demo of Poem Generation
    print 'Dumb Brain Example'
    print '--------------------'
    print parse_and_generate('brains/dumb_poem_brain.md')
    print '--------------------'
    print 'Block Brain Example'
    print '--------------------'
    print parse_and_generate('brains/block_pb.md')
    print '--------------------'
    print 'Theme Brain Example'
    print '--------------------'
    print parse_and_generate('brains/theme_pb.md')
    print '--------------------'
    print 'Wordnet Brain Example'
    print '--------------------'
    print parse_and_generate('brains/wordnet_poem_brain.md')
    print '--------------------'
    print 'Commonsense Graph Brain Example'
    print '--------------------'
    print parse_and_generate('brains/cs_graph_pb.md')
