# coding: utf-8
from pattern.graph.commonsense import Commonsense


class PatternCommonSenseGraph:
    def __init__(self):
        self.sense = Commonsense()

    def taxonomy(self, name):
        concept = self.find_concept(name)
        return self.sense.taxonomy(concept)

    def find_concept(self, name):
        for x in self.sense.concepts:
            if x.id == name:
                return x
        return None
