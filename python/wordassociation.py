# coding: utf-8
import xml.etree.ElementTree
from nltk.corpus import wordnet as wn

class WordAssociation():
	
	def __init__(self):
		self.clear_tables()
		self.associations = xml.etree.ElementTree.parse('eat/eat-stimulus-response.xml').getroot()
		
	
	def clear_tables(self):
		self.verbs = []
		self.nouns = []
		self.adjs = []
		self.satadjs = []
		self.adverbs = []

	def word_association(self, word):
		responses = []
		for atype in self.associations.findall('stimulus[@word="%s"]/response' % word.upper()):
				responses.append(atype.get('word').lower())
		return responses
	
	def load_word_associations(self, word):
		responses =  self.word_association(word)
		for x in responses:
			for y in wn.synsets(x):
				if '.n.' in y.name():
					self.nouns.append(x)
				elif '.v.' in y.name():
					self.verbs.append(x)
				elif '.r.' in y.name():
					self.adverbs.append(x)
				elif '.a.' in y.name():
					self.adjs.append(x)
				elif '.s.' in y.name():
					self.satadjs.append(x)

if __name__ == '__main__':
	wa = WordAssociation()
	wa.load_word_associations('fire')
	print wa.nouns
