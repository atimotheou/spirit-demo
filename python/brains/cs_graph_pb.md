Structure
---------
Title 
Paragraphs 1 to 3
Lines 4 to 7

Title
-----
A poem by a wordnet brain

Lines
-----
An ecstasy so *properties:vast* it has no shore
A craving that *lemma:devours* all decision
A *feeling* for nothingness that lusts for more
There are *heaven_group* in pursuit of pain
Who take Satanic pride in degradation drag you down the *large_object* and back again
Hosanna-ing your sweet humiliation
Just like a *element* fanned by a hot, dry wind
Or like a flood that sweeps away all will
This wall of *feeling* leaves no one behind
No sign of life where all one *feeling* lies still
So *does_negate* the soul in anguish hate the joy
That soothes the hate that *does_negate* the soul destroy
*heaven_group* are quite ample cause to cry
Now, like silent movies, obsolete.
Even as a plaything of the wise.
Lost to all but those that work the street, a retiree not ready yet to die.
*heaven_group* are quite confident
Now they are on high.
Given whom they represent
Each appearing Heaven sent
Let them serve God innocent
As we live and die
Several days before I was to die
A white dove flew into my garden
It had one black spot on its tail
As though a drop of ink had soiled
Its purity. It looked at me
As birds do: head sideways
Neck twisted, almost upside down
Then went the other way, fluttered
Cooed, straightened, and stared at me
With more than human stillness. Our eyes met, and I felt some understanding
Pass between us, as though it sensed
I was to die and felt compassion
I know my own imagination
What we'll never understand
Far surpasses what we know.
Returned from death, am like a boulder
From *element* to *element*

List:does_negate
----
Does
Does not

List:large_object
----
hill
tower
well
eifel
shard

List:heaven_group
----
angels
demons
guardians
spirits
messengers
fiends
creatures

List:feeling
----
lust
love
joy
felicity
purity
tiredness
respect
honor
happiness
wonder
soncerety
decenncy

List:element
----
fire
sky
ice
lightning
water
electricity
heat
lava
clay
mus
earth
wind
jungle
