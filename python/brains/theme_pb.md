Structure
---------
Title 
Paragraphs 1 to 3
Lines 2 to 7

Theme
-----
power

Paragraphs
---------
1 from list start
many from list middle
1 from list end

Writing Style
-----
choose from a list once

Title
-----
A poem of random luck at the start, middle and the end.

Lines start
-----
Within the *noun* comes a threat noun
i'm going to *verb* you
i move like *adverb* *verb*
In the *satadj* *adj*

Lines middle
-----
Within the *noun* comes a threat noun
i'm going to *verb* you
i move like *adverb* *verb*
In the *satadj* *adj*

Lines end
-----
Within the *noun* comes a threat noun
i'm going to *verb* you
i move like *adverb* *verb*
In the *satadj* *adj*

List it is
----
it is
it isn't
it could be
it may be
it may not be
it could not be

List trait
----
positivity
creativity
complexity
kindness
gentleness
evilness
selfishness
discipline

List within
----
within
without

List before
----
before
after
at the start
at the end
in the middle

List it is
----
it is
it is not
it isn't
it won't be
it might
it may well be

List great
----
great
brilliant
wonderful
shining
beautiful

List world
----
world
galaxy
universe
town
city
block
corner

List rocking item
----
rocking chair
pendulum
vortex
mind's eye
power
dominion
sea's tears

List suffice
----
suffice
content in time
make do
make way for the unknown

List dangerous_animal
----
Tiger
Great White Shark
Orca
Killer Whale
Shark
Mountain Lion
Panther
Scorpion
Tarantula

List era
----
viking
90's
80's
70's
60's
stone age
bronze age

List song
----
tune
song
performance

List time of day
----
morning
afternoon
evening
night
dusk
dawn

List moment
----
moment
second
minute
hour
day
week
dawn
dusk

List rainy_country
----
England
Wales
Scotland
Ireland

List deep
----
vast
unimaginable
permeable
timid
timing
powerful
focused

List darkness
----
abyss
darkness
void
whirlpool of black

List person
----
person
people
time
ticket
god
power
slave
fighter
sprinter
officer

List colour
----
turqouise
purple
orange
red
blue
pink
green

List animal
----
tiger
whale
panther
hamster
eagle
bear
lion
shark
squid

List small object
----
bulb
apple
orange
match
pen
paper

List big object
----
hill
tower
well
eifel
shard

List heaven_group
----
angels
demons
guardians
spirits
messengers
fiends
creatures

List feeling
----
lust
love
joy
felicity
purity
tiredness
respect
honor
happiness
wonder
decency

List element
----
fire
sky
ice
lightning
water
electricity
heat
lava
clay
earth
wind
jungle
