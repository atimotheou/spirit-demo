Structure
---------
Title 
Paragraphs 1 to 3
Lines 1 to 7

Title
-----
A poem of random luck

Lines
-----
From *element* to *element*
From *small_object* to *big_object*
The *animal* leaps
On the back of *element*
In the *colour* coloured sky
A *animal* that walks
With a *feeling* projected as trill
The free *person*
Within the *darkness* comes a threat
So lavish, like a *song* of richness
Your eyes glow with a depth so *deep*
Raining through the window, in *rainy_country*
The caged *animal* swells with power
Ready in the *moment*
In the *time of day* a shade begins
A *era* so anxious, it falls.
Songs of *feeling*
Longing for a stillness in the *time of day*
I thank whatever *animal* may be
It matters not what *colour* it drank
How charged the *dangerous_animal* ferocious in nudity.
Some say the world will end in *element*
Some say in *element*
And would *suffice* 
The *rocking item* sways left and right and over again
So *time of day* goes down to *time of day*
I think I know enough of *feeling*
Some say the *world* will end in *element*
The *element* so *deep* it ploughs through
Devouring those around us with sustenance from *heaven_group* 
It is also *great* in it's own right
I know *it is*
*it is* of *element*
A time *before*
A time without *element*
From *within*
Searching for that *trait* in someone that wants to be found
I find within this *trait* a true person forms
In life, I am of *trait*
Good to know *within* it is found
I seek that which is *within*
Go fourth one born of *element*
Listen to me as it plays a *song*
*animal* is but one thing in a sea of many
*animal* fights the battle of survival
An era called *era*
Exotic *it is*
Pure *it is*
Powerful *it is*


List:it is
----
it is
it isn't
it could be
it may be
it may not be
it could not be

List:trait
----
positivity
creativity
complexity
kindness
gentleness
evilness
selfishness
discipline

List:within
----
within
without

List:before
----
before
after
at the start
at the end
in the middle

List:it is
----
it is
it is not
it isn't
it won't be
it might
it may well be

List:great
----
great
brilliant
wonderful
shining
beautiful

List:world
----
world
galaxy
universe
town
city
block
corner

List:rocking item
----
rocking chair
pendulum
vortex
mind's eye
power
dominion
sea's tears

List:suffice
----
suffice
content in time
make do
make way for the unknown

List:dangerous_animal
----
Tiger
Great White Shark
Orca
Killer Whale
Shark
Mountain Lion
Panther
Scorpion
Tarantula

List:era
----
viking
90's
80's
70's
60's
stone age
bronze age

List:song
----
tune
song
performance

List:time of day
----
morning
afternoon
evening
night
dusk
dawn

List:moment
----
moment
second
minute
hour
day
week
dawn
dusk

List:rainy_country
----
England
Wales
Scotland
Ireland

List:deep
----
vast
unimaginable
permeable
timid
timing
powerful
focused

List:darkness
----
abyss
darkness
void
whirlpool of black

List:person
----
person
people
time
ticket
god
power
slave
fighter
sprinter
officer

List:colour
----
turqouise
purple
orange
red
blue
pink
green

List:animal
----
tiger
whale
panther
hamster
eagle
bear
lion
shark
squid

List:small_object
----
bulb
apple
orange
match
pen
paper

List:big_object
----
hill
tower
well
eifel
shard

List:heaven_group
----
angels
demons
guardians
spirits
messengers
fiends
creatures

List:feeling
----
lust
love
joy
felicity
purity
tiredness
respect
honor
happiness
wonder
soncerety
decenncy

List:element
----
fire
sky
ice
lightning
water
electricity
heat
lava
clay
earth
wind
jungle
