# Poem Generation

## Overview
This poem generator allows a user to specify an outline of a poem to be generated in markdown.
Using a number of techniques and functions you can embed within your markdown you can generate more expressive poems.
Such as, look up lists, antonyms, synonyms, common sense graph functions and word association look ups.

## Requirements
- mistune (markdown parser)
- nltk with wordnet downloaded (nltk.download())
- pattern

## Running
Execute the markdownparser.py which will output various poems based on different 'brain' concepts

# Examples

## Simple Brain Example
Pure it may well be   
Go fourth one born of water   
I know it is not   

Some say the world will end in electricity   
In life, I am of kindness   
The caged panther swells with power   
In the evening a shade begins   
Powerful it won't be   

## Black Brain Example
The whale leaps   
Singing songs of felicity   
within it is found   
A finality of focused proportions   

It begins big like a tower   
With a felicity projected as trill   
In the red coloured sky   
A time without water   

## Theme Brain Example
Within the dominion comes a threat scared   
In the set full   
Within the station comes a threat feared   
In the strong mechanical   

## Wordnet Brain Example
Now, like silent movies, obsolete.   
A None None for nothingness that lusts for more   
What we'll never understand   
There is a joy that shuns all reason   
As birds do: head sideways   
Lost to all but those that work the street, a retiree not ready yet to die.   
Cooed, straightened, and stared at me   

Now they are on high.     
This wall of  leaves no one behind   
Far surpasses what we know.   
A white dove flew into my garden   
Several days before I was to die   

Hosanna-ing your sweet humiliation   
It had one black spot on its tail   
Then went the other way, fluttered   
Returned from death, am like a boulder   
I know my own imagination   
I was to die and felt compassion   
Or like a flood that sweeps away all will   

## Commonsense Graph Brain Example
Returned from death, am like a boulder   
No sign of life where all one joy lies still   
Neck twisted, almost upside down   
Given whom they represent   
Just like a fire fanned by a hot, dry wind   
What we'll never understand   

A wonder for nothingness that lusts for more   
Or like a flood that sweeps away all will   
I know my own imagination   
An ecstasy so it has no shore   

Several days before I was to die   
This wall of joy leaves no one behind   
So Does not the soul in anguish hate the joy   
demons are quite ample cause to cry   
A white dove flew into my garden   
Its purity. It looked at me   
