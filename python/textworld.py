# coding: utf-8

import re
import random

from nltk.corpus import wordnet as wn

from pattern.en import parse, Sentence
from pattern.en import lemma, conjugate
from patternwrappers import PatternCommonSenseGraph

from wordassociation import WordAssociation

patternsense = PatternCommonSenseGraph()


class TextWorld:
    def __init__(self, block_list):
        self.title = None
        self.structure = None
        self.randomParagraphsFrom = None
        self.randomParagraphsTo = None
        self.randomLinesFrom = None
        self.randomLinesTo = None
        self.paragraphs = False
        self.repetitions = False
        self.lines = []
        self.lineLists = {}
        self.lists = {}
        self.listRepeat = False
        self.listChoiceHistory = {}
        self.theme = None
        self.wordAssociation = None
        self.parse_block_list(block_list)

    def parseLines(self, listMap):
        lines = listMap[1]['text'].split('\n')

        lineList = []

        for x in lines:
            lineList.append({'text': x})

        self.lines = lineList

    def parseLineList(self, title, listMap):
        index = title[6:]
        lines = listMap[1]['text'].split('\n')

        lineList = []

        for x in lines:
            lineList.append({'text': x})

        self.lineLists[index] = lineList

    def parseLists(self, title, listMap):
        listKey = title[5:]

        lines = listMap[1]['text'].split('\n')

        lineList = []

        for x in lines:
            lineList.append({'text': x})

        self.lists[listKey] = lineList

    def parseStructure(self, listMap):
        structureDesc = listMap[1]['text'].split('\n')

        self.paragraph = {}

        patternFirstNum = re.compile(r':[0-9]:')

        for x in structureDesc:
            splitDesc = x.split(' ')
            if x.startswith('Paragraph'):
                self.randomParagraphsFrom = int(splitDesc[1])
                self.randomParagraphsTo = int(splitDesc[3])
            elif x.startswith('Lines'):
                self.randomLinesFrom = int(splitDesc[1])
                self.randomLinesTo = int(splitDesc[3])

    def parseTitle(self, listMap):
        self.title = listMap[1]['text']

    def parseParagraphs(self, block):
        paragraphDesc = block[1]['text'].split('\n')

        lineArray = paragraphDesc[0].split(' ')
        listStartIndex = paragraphDesc[0].find('list') + 5
        self.paraStartLines = lineArray[0]
        self.paraStartList = paragraphDesc[0][listStartIndex:]

        lineArray = paragraphDesc[1].split(' ')
        listStartIndex = paragraphDesc[1].find('list') + 5
        self.paraMiddleLines = lineArray[0]
        self.paraMiddleList = paragraphDesc[1][listStartIndex:]

        lineArray = paragraphDesc[1].split(' ')
        listStartIndex = paragraphDesc[2].find('list') + 5
        self.paraMiddleLines = lineArray[0]
        self.paraMiddleList = paragraphDesc[2][listStartIndex:]

        self.paragraphs = True

    def parseWritingStyle(self, block):
        writingStyleDesc = block[1]['text'].split('\n')

        for x in writingStyleDesc:
            if x == 'choose from a list once':
                self.listRepeat = True

    def parseTheme(self, block):
        themesDesc = block[1]['text'].split('\n')

        self.theme = random.choice(themesDesc)
        self.wordAssociation = WordAssociation()
        self.wordAssociation.load_word_associations(self.theme)

    def parse_block_list(self, block_list):
        for x in block_list:
            title = x[0]['title'].lower()
            if title == 'structure':
                self.parseStructure(x)
            elif title == 'title':
                self.parseTitle(x)
            elif title == 'lines':
                self.parseLines(x)
            elif title.startswith('lines '):
                self.parseLineList(title, x)
            elif title == 'paragraphs':
                self.parseParagraphs(x)
            elif title == 'writing style':
                self.parseWritingStyle(x)
            elif title == 'theme':
                self.parseTheme(x)
            elif title.startswith('list ') or title.startswith('list:'):
                self.parseLists(title, x)

    def processInlineCommands(self, text):
        command_list = list([LemmaCommand()
                                , HypernymCommand()
                                , AntonymCommand()
                                , HypernymCommand()
                                , HyponymCommand()
                                , MemberHolonymCommand()
                                , RootHypernymCommand()
                                , TopicDomainCommand()
                                , UsageDomainCommand()
                                , RegionDomainCommand()
                                , EntailmentCommand()
                                , SimilarToCommand()
                                , AttributeCommand()
                                , NounCommand()
                                , VerbCommand()
                                , AdverbCommand()
                                , AdjectiveCommand()
                                , SatelliteAdjectiveCommand()
                             ])

        # replace tokens within * with underscores
        commandProcessedString = ''
        commandToggle = False

        for x in text:
            if x == '*':
                commandToggle = not commandToggle

            if x == ' ' and commandToggle:
                commandProcessedString += '__'
            else:
                commandProcessedString += x

        textTokens = commandProcessedString.split(' ')
        processedTokens = ''
        for x in textTokens:
            if x.startswith('*') and x.endswith('*'):
                commandToken = x.replace('__', ' ')
                if commandToken.find(':') > 0:
                    commandIndex = commandToken.find(':')
                    commandName = commandToken[1:commandIndex].upper()
                    commandValue = commandToken[commandIndex:-1]

                    for command in command_list:
                        if command.name == commandName:
                            value = command.process(x.upper(), self)
                            processedTokens += str(value) + ' '
                else:
                    listkey = commandToken[1:-1]

                    commandFound = False
                    commandName = listkey.upper()
                    for command in command_list:
                        if command.name == commandName:
                            value = command.process(x.upper(), self)
                            commandFound = True
                            processedTokens += str(value) + ' '

                    if not commandFound:
                        if self.listRepeat and self.listChoiceHistory.get(listkey):
                            randomWord = self.listChoiceHistory[listkey]
                            processedTokens += randomWord + ' '
                        else:
                            randomWord = random.choice(self.lists[listkey])
                            self.listChoiceHistory[listkey] = randomWord['text']
                            processedTokens += randomWord['text'] + ' '
            else:
                processedTokens += x + ' '

        return processedTokens

    def getLineFromList(self, index):
        item = random.choice(self.lineLists[index])

        processedText = self.processInlineCommands(item['text'])

        if self.repetitions == False and len(self.lineLists[index]) > 1:
            self.lineLists[index].remove(item)

        return processedText

    def generateWhatToSay(self):
        themes = ['Fire', 'Peace', 'War']
        return random.choice(themes)

    # the how to say it
    def generate(self):
        theme = self.generateWhatToSay()
        text = ''

        numberOfParagraphs = random.randint(self.randomParagraphsFrom, self.randomParagraphsTo)

        for x in range(numberOfParagraphs):
            numberOfSentences = random.randint(self.randomLinesFrom, self.randomLinesTo)

            if self.paragraphs:
                # assign first
                start = self.getLineFromList('start')
                text += start + '\n'

                # assign middle
                for y in range(numberOfSentences - 2):
                    sentence = self.getLineFromList('middle')
                    text += sentence + '\n'

                # assign end
                end = self.getLineFromList('end')
                text += end + '\n'
            else:
                for y in range(numberOfSentences):
                    item = random.choice(self.lines)

                    processedText = self.processInlineCommands(item['text'])

                    if self.repetitions == False and len(self.lines) > 1:
                        self.lines.remove(item)

                    text += processedText + '\n'

            text += '\n'

        return text


# Helper methods
def get_all_synsets(token):
    lemmaValue = token[token.index(':') + 1:-1]
    return wn.synsets(lemmaValue)


def get_synset(token):
    # TODO - clever logic around getting synsets, add sentence in
    return get_all_synsets(token)[0]


def get_random_lemma(synset):
    return random.choice(synset.lemmas())


def get_token_value(token):
    return token[token.index(':') + 1:-1]


def get_relations(concept, type):
    list = []

    for x in concept.edges:
        if x.type == type:
            if x.node1.id != concept.id:
                list.append(x.node1.id)
            elif type == 'is-opposite-of' and x.node2.id != concept.id:
                list.append(x.node2.id)
            elif type == 'is-effect-of':
                list.append(x.node2.id)

    if len(list) == 0:
        list.append('NOT_FOUND')

    return list


def random_choice_or_skip(array):
    if len(array) == 0:
        return '[SKIP_LINE]'
    else:
        return random.choice(array)


# Command Classes
# Theme commands
class NounCommand:
    def __init__(self):
        self.name = "NOUN"

    def process(self, token, textworld):
        return random_choice_or_skip(textworld.wordAssociation.nouns)


class VerbCommand:
    def __init__(self):
        self.name = "VERB"

    def process(self, token, textworld):
        return random_choice_or_skip(textworld.wordAssociation.verbs)


class AdverbCommand:
    def __init__(self):
        self.name = "ADVERB"

    def process(self, token, textworld):
        return random_choice_or_skip(textworld.wordAssociation.adverbs)


class AdjectiveCommand:
    def __init__(self):
        self.name = "ADJ"

    def process(self, token, textworld):
        return random_choice_or_skip(textworld.wordAssociation.adjs)


class SatelliteAdjectiveCommand:
    def __init__(self):
        self.name = "SATADJ"

    def process(self, token, textworld):
        return random_choice_or_skip(textworld.wordAssociation.satadjs)


# Commonsense Graph from Pattern commands
class TaxonomyCommand:
    def __init__(self):
        self.name = "TAX"

    def process(self, token, textworld):
        concept_name = get_token_value(token)
        concepts = patternsense.taxonomy(concept_name)
        random_concept = random.choice(concepts)
        return random_concept.id


class PropertiesCommand:
    def __init__(self):
        self.name = "PROPERTIES"

    def process(self, token, textworld):
        concept_name = get_token_value(token)
        concept = patternsense.find_concept(concept_name)
        random_concept = random.choice(concept.properties)
        return random_concept


class IsACommand:
    def __init__(self):
        self.name = "is a"

    def process(self, token, textworld):
        concept_name = get_token_value(token)
        concept = patternsense.find_concept(concept_name)
        relations = get_relations(concept, 'is-a')
        random_concept = random.choice(relations)
        return random_concept


class IsPartOfCommand:
    def __init__(self):
        self.name = "is part of "

    def process(self, token, textworld):
        concept_name = get_token_value(token)
        concept = patternsense.find_concept(concept_name)
        relations = get_relations(concept, 'is-part-of')
        random_concept = random.choice(relations)
        return random_concept


class IsOppositeOfCommand:
    def __init__(self):
        self.name = "is opposite of"

    def process(self, token, textworld):
        concept_name = get_token_value(token)
        concept = patternsense.find_concept(concept_name)
        relations = get_relations(concept, 'is-opposite-of')
        random_concept = random.choice(relations)
        return random_concept


class IsPropertyOfCommand:
    def __init__(self):
        self.name = "is property of"

    def process(self, token, textworld):
        concept_name = get_token_value(token)
        concept = patternsense.find_concept(concept_name)
        relations = get_relations(concept, 'is-property-of')
        random_concept = random.choice(relations)
        return random_concept


class IsRelatedToCommand:
    def __init__(self):
        self.name = "is related to"

    def process(self, token, textworld):
        concept_name = get_token_value(token)
        concept = patternsense.find_concept(concept_name)
        relations = get_relations(concept, 'is-related-to')
        random_concept = random.choice(relations)
        return random_concept


class IsSameAsCommand:
    def __init__(self):
        self.name = "is same as"

    def process(self, token, textworld):
        concept_name = get_token_value(token)
        concept = patternsense.find_concept(concept_name)
        relations = get_relations(concept, 'is-same-as')
        random_concept = random.choice(relations)
        return random_concept


class IsEffectOfCommand:
    def __init__(self):
        self.name = "is effect of"

    def process(self, token, textworld):
        concept_name = get_token_value(token)
        concept = patternsense.find_concept(concept_name)
        relations = get_relations(concept, 'is-effect-of')
        random_concept = random.choice(relations)
        return random_concept


# Wordnet commands
class AttributeCommand:
    def __init__(self):
        self.name = "ATTRIBUTE"

    def process(self, token, textworld):
        synset = get_synset(token)
        attribute = random.choice(synset.attributes())
        return attribute.name()


class SimilarToCommand:
    def __init__(self):
        self.name = "SIMILAR_TO"

    def process(self, token, textworld):
        synset = get_synset(token)
        similar_to = random.choice(synset.similar_tos())
        return similar_to.name()


class EntailmentCommand:
    def __init__(self):
        self.name = "ENTAILMENT"

    def process(self, token, textworld):
        synset = get_synset(token)
        entailment = random.choice(synset.entailments())
        return entailment.name()


class RegionDomainCommand:
    def __init__(self):
        self.name = "REGION_DOMAIN"

    def process(self, token, textworld):
        synset = get_synset(token)
        region_domain = random.choice(synset.region_domains())
        return region_domain.name()


class UsageDomainCommand:
    def __init__(self):
        self.name = "USAGE_DOMAIN"

    def process(self, token, textworld):
        synset = get_synset(token)
        usage_domain = random.choice(synset.usage_domains())
        return usage_domain.name()


class TopicDomainCommand:
    def __init__(self):
        self.name = "TOPIC_DOMAINS"

    def process(self, token, textworld):
        synset = get_synset(token)
        topic_domain = random.choice(synset.topic_domains())
        return topic_domain.name()


class RootHypernymCommand:
    def __init__(self):
        self.name = "ROOT_HYPERNYM"

    def process(self, token, textworld):
        synset = get_synset(token)
        root_hypernyms = random.choice(synset.root_hypernyms())
        return root_hypernyms.name()


class MemberHolonymCommand:
    def __init__(self):
        self.name = "MEMBER_HOLONYM"

    def process(self, token, textworld):
        synset = get_synset(token)
        holonym = random.choice(synset.member_holonyms())
        return holonym.name()


class HyponymCommand:
    def __init__(self):
        self.name = "HYPONYM"

    def process(self, token, textworld):
        synset = get_synset(token)
        hyponym = random.choice(synset.hyponyms())
        return hyponym.name()


class HypernymCommand:
    def __init__(self):
        self.name = "HYPERNYM"

    def process(self, token, textworld):
        synset = get_synset(token)
        hypernym = random.choice(synset.hypernyms())
        return hypernym.name()


class AntonymCommand:
    def __init__(self):
        self.name = "ANTONYM"

    def process(self, token, textworld):
        synset = get_synset(token)
        lemma = get_random_lemma(synset)
        anytonym = lemma.antonyms()
        return anytonym.name().replace('_', ' ')


class LemmaCommand:
    def __init__(self):
        self.name = "LEMMA"

    def process(self, token, textworld):
        synset = get_synset(token)
        lemma = get_random_lemma(synset)
        # TODO - Refactor into own logic that maps the values well
        # need to get tag of part of speech and then add that after, such as third person
        lemmaValue = token[token.index(':') + 1:-1]
        chosenSimilarWord = ""
        if 'VBZ' in parse(lemmaValue):
            chosenSimilarWord = conjugate(lemma.name(), '3sg')
        return chosenSimilarWord.replace('_', ' ')


class HypernymCommand:
    def __init__(self):
        self.name = "HYPERNYM"

    def process(self, token, textworld):
        synset = get_synset(token)
        random.choice(synset.hypernyms()).name()
